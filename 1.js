document.addEventListener('DOMContentLoaded', function() {
    const button = document.querySelector('.theme');
    const css1 = document.getElementById('css1');
    const css2 = document.getElementById('css2');

    // Проверяем, есть ли сохраненное значение в LocalStorage
    const savedTheme = localStorage.getItem('theme');
    if (savedTheme === 'toggle') {
        // Если сохраненная тема - "toggle", применяем соответствующие стили
        css1.href = 'toggle.css';
        css2.href = 'style.css';
    } else {
        // Иначе применяем обычные стили
        css1.href = 'style.css';
        css2.href = 'toggle.css';
    }

    button.addEventListener("click", () => {
        if (css1.href.includes('toggle.css')) {
            css1.href = 'style.css';
            css2.href = 'toggle.css';
            // Сохраняем текущую тему в LocalStorage
            localStorage.setItem('theme', 'default');
        } else {
            css1.href = 'toggle.css';
            css2.href = 'style.css';
            // Сохраняем текущую тему в LocalStorage
            localStorage.setItem('theme', 'toggle');
        }
    });
});